import UIKit

// Enumeradores
enum Compass: String {
    case north = "Norte"
    case south = "Sul"
    case east = "Leste"
    case west = "Oeste"
}

var heading: Compass = .north

print("Estou indo para o \(heading.rawValue)")

switch (heading){
    case .north:
        print("Estou indo para o \(heading)")
    case .south:
        print("Estou indo para o \(heading)")
    case .east:
        print("Estou indo para o \(heading)")
    case .west:
        print("Estou indo para o \(heading)")
}


enum Measure {
    case age(Int)
    case weight(Double)
    case size(width: Double, height: Double)
}

//var measure : Measure = .age(29)
var measure : Measure = .size(width: 25, height: 33)


switch measure {
case .age(let idade):
    print("Esta é uma medida de idade, e a idade é \(idade)")
case .weight:
    print("Esta é uma medida de peso")
case .size(let size):
    print("Esta é uma medida de tamanho, cuja largura é \(size.width)cm e a altura é \(size.height)cm")
}

//Estruturas
var eric : String = "Eric"
var pedro = eric

eric = "João"
print(pedro)


//Estrutura propria

struct Person {
    var name : String
    
}

var person1 = Person(name: "JUSTÉIN")
var person2 = person1;


person1.name = "Beliber"
print(person1.name)
print(person2.name)


import Foundation
    pow(2, 3)
    sqrt(64)

func say(sentence : String) {
    print(sentence)
}

say(sentence: "OLA LIXOS")

func soma(_ numero1: Int, com numero2: Int) -> Int {
    return numero1 + numero2
}

var conta = soma(5, com: 95)
print(conta)


func sum(a: Int, b : Int) -> Int {
    return a+b
}

func substract(a: Int, b : Int) -> Int {
    return a-b
}

func multiply(a: Int, b : Int) -> Int {
    return a*b
}

func divide(a: Int, b : Int) -> Int {
    return a/b
}

typealias Operation = (Int, Int) -> Int
func calculate(a: Int, b: Int, operation: Operation) -> Int {
    return operation(a, b)
}

print(calculate(a: 20, b: 20, operation: sum))
