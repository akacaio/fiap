package br.com.fiap.bean;

import java.util.Calendar;

public class ContaCorrente extends Conta {
	
	private TipoConta tipo;

	public ContaCorrente() {}
	
	public ContaCorrente(TipoConta tipo) {
		setTipoConta(tipo);
	}
	
	public ContaCorrente(int agencia, int numero, Calendar dataAbertura, double saldo, TipoConta tipo) {
		super(agencia, numero, dataAbertura, saldo);
		this.tipo = tipo;
	}

	public TipoConta getTipoConta() {
		return tipo;
	}

	public void setTipoConta(TipoConta tipo) {
		this.tipo = tipo;
	}

	@Override
	public double depositar(double valor) {
		return saldo += valor;
	}

	@Override
	public double retirar(double valor) throws Exception {
		if(tipo == TipoConta.COMUM && saldo < valor) {
			throw new Exception("Saque n�o permitido.");
		}
		return saldo -= valor;
	}





}
