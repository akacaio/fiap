package br.com.fiap.bean;

import java.util.Calendar;

public class ContaPoupanca extends Conta implements ContaInvestimento {
	
	public static final float TAXA = 10;
	public static final float RENDIMENTO = 0.5f;

	public ContaPoupanca() {
		super();
	}

	public ContaPoupanca(int agencia, int numero, Calendar dataAbertura, double saldo) {
		super(agencia, numero, dataAbertura, saldo);
	}
	
	@Override
	public double calculaInvestimento(double valor) {
		return saldo * RENDIMENTO;
	}

	@Override
	public double depositar(double valor) {
		return super.getSaldo() + valor;
	}

	@Override
	public double retirar(double valor) throws Exception {
		if(saldo < valor) {
			throw new Exception("Saldo insuficiente.");
		}
		return saldo = (saldo - valor) - TAXA;
	}
}
