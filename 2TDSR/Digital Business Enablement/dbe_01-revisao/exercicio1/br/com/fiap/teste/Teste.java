package br.com.fiap.teste;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import br.com.fiap.bean.ContaCorrente;
import br.com.fiap.bean.ContaPoupanca;
import br.com.fiap.bean.TipoConta;

public class Teste {

	public static void main(String[] args) throws Exception {
		
		ContaCorrente cc = new ContaCorrente();
		Calendar dtCalendario = new GregorianCalendar(2010, Calendar.SEPTEMBER, 20);
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		
		cc.setAgencia(900);
		cc.setNumero(01017153);
		cc.setSaldo(2000);
		cc.setTipoConta(TipoConta.PREMIUM);
		cc.setDataAbertura(dtCalendario);
		
		
//		double retirado = cc.retirar(300);
//		System.out.println("Saldo ap�s retirada: " + retirado);
//		double deposito = cc.depositar(500);
//		System.out.println("Saldo ap�s dep�sito: " + deposito);
	
		
		System.out.println("INFORMA��ES CONTA POUPAN�A\n" + 
				         "\nAg�ncia: " + cc.getAgencia() + 
				         "\nN�mero da Conta: " + cc.getNumero() +
				         "\nSaldo: " + cc.getSaldo() +
				         "\nTipo da Conta: " + cc.getTipoConta() + 
				         "\nData de Abertura: " + fmt.format(cc.getDataAbertura().getTime())
				);
		
		ContaPoupanca cp = new ContaPoupanca();
		
		cp.setAgencia(900);
		cp.setNumero(10248536);
		cp.setSaldo(5000);
		cp.setDataAbertura(dtCalendario);
		
		double resultado = cp.calculaInvestimento(cp.getSaldo());
		System.out.println("\nINFORMA��ES CONTA POUPAN�A\n" + 
				         "\nAg�ncia: " + cp.getAgencia() + 
				         "\nN�mero da Conta: " +cp.getNumero() + 
				         "\nSaldo: " + cp.getSaldo() + 
				         "\nData de Abertura:" + fmt.format(cp.getDataAbertura().getTime()) + 
				         "\nInvestimento R$: "+resultado);
		
		List<ContaCorrente> listaContas = new ArrayList<ContaCorrente>();
		listaContas.add(new ContaCorrente(900,123457, new GregorianCalendar(2009, Calendar.SEPTEMBER, 20),100,TipoConta.COMUM));
		listaContas.add(new ContaCorrente(900,789123, new GregorianCalendar(1977, Calendar.NOVEMBER, 14),100,TipoConta.ESPECIAL));
		listaContas.add(new ContaCorrente(900,654321, new GregorianCalendar(1998, Calendar.APRIL, 17),100,TipoConta.PREMIUM));

		for (ContaCorrente contac : listaContas) {
			System.out.println("\n"+ contac.toString());
		}
		
	}
}
