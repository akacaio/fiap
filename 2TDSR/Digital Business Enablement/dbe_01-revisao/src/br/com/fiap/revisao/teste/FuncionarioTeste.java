package br.com.fiap.revisao.teste;

import br.com.fiap.revisao.bean.Funcionario;
import br.com.fiap.revisao.bean.Gerente;

public class FuncionarioTeste {
	
	public static void main(String[] args) {
		
//		Funcionario func = new Funcionario();
//		func.setSalario(900);
		
		Funcionario func = new Gerente();
		func.setSalario(900);
		
		Gerente mngr = (Gerente) func; //cast para alterar o tipo de dado
		
		System.out.println(func.calcBonus() + "\n" + mngr.calcBonus());
		String salario = (func.getSalario() > 950)?"Maior":"Menor";
		System.out.println(salario);
	}

}
