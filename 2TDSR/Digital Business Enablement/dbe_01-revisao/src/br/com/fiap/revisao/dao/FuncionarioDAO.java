package br.com.fiap.revisao.dao;
import br.com.fiap.revisao.bean.Funcionario;


/* voc� pode herdar uma ou mais interfaces, 
 * mas n�o pode implementar outras interfaces
 */
public interface FuncionarioDAO {
	
	
	void cadastrar(Funcionario func);
	void atualizar(Funcionario func);
	void remover(int id);
	
	

}
