package br.com.fiap.revisao.teste;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ColecaoTeste {
	
	public static void main(String[] args) {
		
		List<String> lista = new ArrayList<String>();
		lista.add("Ol� Mundo");
		lista.add("Hola Mundo");
		lista.add("Hello World");
		lista.add("Ciao Mondo");
		lista.add("Hallo Welt");
	
	
	for (String item : lista) {
		System.out.println(item);
	}
	
	System.out.println("SET");
	Set<String> set = new HashSet<>();
	
	//adicionar valores no set
	
	set.add("Strogonoff");
	set.add("Pizza");
	set.add("Pizza");
	
	// exibir os elementos do set
	for (String item : set) {
		System.out.println(item);
	}
	
	//MAPA<Chave,Valor>
	System.out.println("MAPA");
	Map<String, String> mapa = new HashMap<>();
	
	//adicionar valores no map
	mapa.put("Brasil", "Brasilia");
	mapa.put("Argentina", "Buenos Aires");
	mapa.put("Venezuela", "Caracas");
	
	System.out.println(mapa.get("Venezuela"));
	
	}
}
