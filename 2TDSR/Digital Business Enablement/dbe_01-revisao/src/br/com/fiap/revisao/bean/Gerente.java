package br.com.fiap.revisao.bean;

public class Gerente extends Funcionario {
	
	//sobrescrita do m�todo
	
	@Override
	public double calcBonus() {
		return salario * 2;
	}	
	
	//chamado quando o objeto � passado no sysout
	@Override
	public String toString() {
		return "Sal�rio: "+ salario;
	}

}
