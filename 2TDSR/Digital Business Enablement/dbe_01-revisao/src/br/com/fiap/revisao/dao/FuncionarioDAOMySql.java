package br.com.fiap.revisao.dao;

import br.com.fiap.revisao.bean.Funcionario;

/* pode herdar uma �nica classe, 
 * e pode implementar uma ou mais interfaces
 */

public class FuncionarioDAOMySql implements FuncionarioDAO {

	@Override
	public void cadastrar(Funcionario func) {

	}

	@Override
	public void atualizar(Funcionario func) {

	}

	@Override
	public void remover(int id) {

	}

}
