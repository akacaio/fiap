package br.com.fiap.singleton;
import java.io.IOException;
import java.util.Properties;

public class ConfigSingleton {
	
	// 1- ATRIBUTO EST�TICO QUE ARMAZENA UM �NICO OBJETO
	private static Properties pprt;
	
	//2- M�TODO EST�TICO QUE RETORNA O �NICO OBJETO
	public static Properties getPprtInstance() {
		if(pprt == null) {
			pprt = new Properties();
			//carregar o objeto com as configura��es do arquivo
			try {
				pprt.load(ConfigSingleton.class.getResourceAsStream("/app.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return pprt;
	}
 
	//3- CONSTRUTOR PRIVADO
	private ConfigSingleton() {}
	
}
