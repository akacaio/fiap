package br.com.fiap.teste;

import org.apache.log4j.Logger;

import br.com.fiap.singleton.ConfigSingleton;

public class Teste {
	
	
	//cria o objeto Logger
	private static Logger log = Logger.getLogger(Teste.class);
	
	public static void main(String[] args) {
		
		log.trace("Aplica��o Iniciada");
		
		//Recuperar o idioma da aplica��o
		String property = ConfigSingleton.getPprtInstance().getProperty("idioma");
		log.info("Recuperando o idioma do sistema: " + property);
		System.out.println(property);
		
	}

}
