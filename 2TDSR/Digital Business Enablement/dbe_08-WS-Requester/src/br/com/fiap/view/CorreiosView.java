package br.com.fiap.view;

import java.util.Scanner;

import org.tempuri.CalcPrecoPrazoWSStub;
import org.tempuri.CalcPrecoPrazoWSStub.CServico;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazo;
import org.tempuri.CalcPrecoPrazoWSStub.CalcPrazoResponse;

public class CorreiosView {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		String sedex = "40010";
		
		// cep 1
		System.out.println("CEP Remetente");
		String remetente = sc.next();
		
		System.out.println("CEP Destinat�rio");
		String destino = sc.next();
		
		try {
			
			CalcPrecoPrazoWSStub stub = new CalcPrecoPrazoWSStub();
			CalcPrazo prazo = new CalcPrazo();
			
			prazo.setSCepOrigem(remetente);
			prazo.setSCepDestino(destino);
			prazo.setNCdServico(sedex);
			
			CalcPrazoResponse resp = stub.calcPrazo(prazo);
			CServico cservico = resp.getCalcPrazoResult().getServicos().getCServico()[0];
			
			System.out.println("Seu prazo de entrega � at�: " + cservico.getPrazoEntrega() + " dias");
			System.out.println("Data M�xima para entraga: "+ cservico.getDataMaxEntrega());
			
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
		
		
		
		
		
	}

}
