package br.com.fiap.loja;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

import org.apache.log4j.Logger;

import br.com.fiap.loja.bo.EstoqueBO;
import br.com.fiap.loja.exception.ProdutoNaoEncontradoException;
import br.com.fiap.loja.to.ProdutoTO;

public class TerminalConsulta {
	
	//logger
	private static Logger log = Logger.getLogger(TerminalConsulta.class);	

	//camada de apresenta��o
	public static void main(String[] args) {
		
		Scanner tec = new Scanner(System.in);
		EstoqueBO bo = new EstoqueBO();
		Calendar data = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		System.out.println("Codinhos Hub" + sdf.format(data.getTime()));
		System.out.println("digite o c�digo: ");
		int codigo = tec.nextInt();
		
		log.warn("C�digo digitado: " + codigo);
		
		try {
			ProdutoTO produto = bo.consultarProduto(codigo);
			System.out.println(produto.getDescricao());
			System.out.println("Pre�o: " + produto.getPreco());
			System.out.println("Qnt: " + produto.getQuantidade());
		} catch (ProdutoNaoEncontradoException e) {
			System.err.println("Produto n�o encontrado");
			log.error("Produto n�o existente");
		}
		
		tec.close();
		
	}
	
}
