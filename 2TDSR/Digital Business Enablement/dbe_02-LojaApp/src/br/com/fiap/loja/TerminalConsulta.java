package br.com.fiap.loja;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;

public class TerminalConsulta {

	public static void main(String[] args) {
		
		int var = 0;

		do {
		int tec = Integer.parseInt(JOptionPane.showInputDialog("Digite o C�digo"));
		Date dataAtual = Calendar.getInstance().getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		if(tec == 401) {
			System.out.println("\nFIAP STORE - "+ sdf.format(dataAtual)
					+ "\nC�digo: " + tec 
					+ "\nProduto: Camiseta Branca.");
			var = 1;
		} else if(tec == 402) {
			System.out.println("\nFIAP STORE - "+ sdf.format(dataAtual)
					+ "\nC�digo: " + tec 
					+ "\nProduto: Camiseta Azul.");
			var = 1;
		} else if(tec == 403) {
			System.out.println("\nFIAP STORE - "+ sdf.format(dataAtual)
					+ "\nC�digo: " + tec 
					+ "\nProduto: Camiseta Rosa.");
			var = 1;
		} else {
			System.out.println("\nFIAP STORE - "+ sdf.format(dataAtual)
					+ "\nC�digo: " + tec 
					+ "\nProduto n�o encontrado.");
	}
		
		} while (var == 0);

 }
}
