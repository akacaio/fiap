package br.com.fiap.entity;

public enum Massa {
	
	FINA, GROSSA, GOURMET, INTEGRAL, VEGANA, DOCE, CANABICA;

}
