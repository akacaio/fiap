package br.com.fiap.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.entity.Massa;
import br.com.fiap.entity.Pastel;

public class AtualizaTeste {
	
	public static void main(String[] args) {
		
	
	
	EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
	EntityManager em = fabrica.createEntityManager();
	
	Pastel pastel = new Pastel(1, "Queijo", 5, false, Massa.CANABICA, new GregorianCalendar(2019, Calendar.AUGUST, 20), null);
	
	em.merge(pastel);
	em.getTransaction().begin();
	em.getTransaction().commit();		
	em.close();
	
	fabrica.close();
	
	
	}
}
