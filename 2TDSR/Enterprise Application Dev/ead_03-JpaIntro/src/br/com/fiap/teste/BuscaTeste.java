package br.com.fiap.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.entity.Pastel;

public class BuscaTeste {
	
	public static void main(String[] args) {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		EntityManager em = factory.createEntityManager();
				
		//Pesquisar um pastel de id = 1
		Pastel pastel = em.find(Pastel.class, 1);
		
		System.out.println("Sabor:" +pastel.getSabor());
		pastel.setSabor("Carne com Queijo");
		
		em.getTransaction().begin();
		em.getTransaction().commit();
		em.close();
		
		factory.close();
		
	}

}
