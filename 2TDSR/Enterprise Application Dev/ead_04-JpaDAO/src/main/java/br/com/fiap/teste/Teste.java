package br.com.fiap.teste;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.fiap.dao.FarmaciaDAO;
import br.com.fiap.dao.impl.FarmaciaIMPL;
import br.com.fiap.entity.Farmacia;
import br.com.fiap.entity.TipoFarmacia;
import br.com.fiap.exception.CodigoInvalidoException;

public class Teste {
	
	public static void main(String[] args) {
		
		EntityManagerFactory fabrica = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		EntityManager em = fabrica.createEntityManager();	
		
		FarmaciaDAO fDAO = new FarmaciaIMPL(em);
		
		//cadastrar uma farm�cia
		Farmacia farmacia = new Farmacia("Drogasil", new GregorianCalendar(2019, Calendar.APRIL,20),TipoFarmacia.MANIPULA��O, true);
		
		try {
			fDAO.cadastrar(farmacia);
			fDAO.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// ATUALIZAR
		farmacia.setNome("Ultrafarma");
		try {
			fDAO.atualizar(farmacia);
			fDAO.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// BUSCAR
		
		Farmacia buscaDrogaria = fDAO.buscar(1);
		System.out.println(buscaDrogaria.getNome());
	
	// DELETE
		try {
		fDAO.deletar(1);
		fDAO.commit();
		} catch (CodigoInvalidoException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
