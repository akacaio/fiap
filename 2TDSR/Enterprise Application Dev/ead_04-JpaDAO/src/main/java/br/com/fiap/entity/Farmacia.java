package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
	 * TB_FARMACIA
	 * CD_FARMACIA (NUMBER) (PK)
	 * NM_FARMACIA (VARCHAR2)
	 * DT_ABERTURA (DATE)
	 * DS_TIPO (INTEGER) (ENUM)
	 * DS_PLANTAO (NUMBER)
	 */
@Entity
@Table(name="TB_FARMACIA")
@SequenceGenerator(name="farmacia", sequenceName = "seq_DROGARIA", allocationSize= 1)
public class Farmacia {

	public Farmacia() {
		super();
	}
	
	public Farmacia(int codigo, String nome, Calendar dtAbertura, TipoFarmacia tipo, boolean is24horas) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.dtAbertura = dtAbertura;
		this.tipo = tipo;
		this.is24horas = is24horas;
	}
	
	public Farmacia(String nome, Calendar dtAbertura, TipoFarmacia tipo, boolean is24horas) {
		super();
		this.nome = nome;
		this.dtAbertura = dtAbertura;
		this.tipo = tipo;
		this.is24horas = is24horas;
	}

	@Id
	@GeneratedValue(generator = "farmacia", strategy = GenerationType.SEQUENCE)
	@Column(name="CD_FARMACIA")
	protected int codigo;

	@Column(name="NM_FARMACIA", nullable = false, length = 50)
	protected String nome;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DT_ABERTURA", nullable = false, updatable = false)
	protected Calendar dtAbertura;
	
	@Enumerated(EnumType.STRING)
	@Column(name="DS_TIPO", nullable = false, length = 50)
	protected TipoFarmacia tipo;
	
	@Column(name="DS_PLANTAO", nullable = false)
	protected boolean is24horas;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDtAbertura() {
		return dtAbertura;
	}

	public void setDtAbertura(Calendar dtAbertura) {
		this.dtAbertura = dtAbertura;
	}

	public TipoFarmacia getTipo() {
		return tipo;
	}

	public void setTipo(TipoFarmacia tipo) {
		this.tipo = tipo;
	}

	public boolean isIs24horas() {
		return is24horas;
	}

	public void setIs24horas(boolean is24horas) {
		this.is24horas = is24horas;
	}
	
}
