package br.com.fiap.dao;

import br.com.fiap.entity.Farmacia;
import br.com.fiap.exception.CodigoInvalidoException;
import br.com.fiap.exception.CommitException;

public interface FarmaciaDAO {
	
	Farmacia buscar(int codigo);
	void cadastrar(Farmacia farmacia);
	void atualizar(Farmacia farmacia);
	void deletar(int codigo) throws CodigoInvalidoException;
	void commit() throws CommitException;

}
