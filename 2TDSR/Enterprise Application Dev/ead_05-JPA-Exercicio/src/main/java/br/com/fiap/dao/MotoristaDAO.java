package br.com.fiap.dao;

import br.com.fiap.entity.Motorista;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.InvalidCodeException;

public interface MotoristaDAO {

	Motorista busca (int nr_carteira);
	void cadastrar (Motorista moto);
	void atualizar (Motorista moto);
	void deletar (int nr_carteira) throws InvalidCodeException;
	void commit() throws CommitException;
	
	
}
