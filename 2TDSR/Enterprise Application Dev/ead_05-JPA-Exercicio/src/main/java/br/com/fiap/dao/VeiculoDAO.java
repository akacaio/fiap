package br.com.fiap.dao;

import br.com.fiap.entity.Veiculo;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.InvalidCodeException;

public interface VeiculoDAO {
	
	
	void cadastrar (Veiculo veiculo);
	void atualizar (Veiculo veiculo);
	void remover (int cd_veiculo) throws InvalidCodeException;
	Veiculo buscar(int cd_veiculo);
	void commit() throws CommitException;	

}
