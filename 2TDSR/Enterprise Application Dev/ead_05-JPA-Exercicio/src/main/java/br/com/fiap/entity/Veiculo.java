package br.com.fiap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="T_VEICULO")
@SequenceGenerator(name = "sq_veiculo", sequenceName="SQ_T_VEICULO" ,allocationSize=1)
public class Veiculo {

	@Id
	@GeneratedValue(generator="sq_veiculo", strategy = GenerationType.SEQUENCE)
	@Column(name="CD_VEICULO")
	private int cd_veiculo;
	
	
	@Column(name="DS_PLACA", nullable = false, length= 9)
	private String ds_placa;
	
	@Column(name="DS_COR", nullable = false, length= 20)
	private String ds_cor;
	
	@Column(name="NR_ANO", updatable = false, nullable = false)
	private int nr_ano;
	
	public Veiculo(){ }
	
	public Veiculo(String ds_placa, String ds_cor, int nr_ano) {
		super();
		this.ds_placa = ds_placa;
		this.ds_cor = ds_cor;
		this.nr_ano = nr_ano;
	}

	public Veiculo(int cd_veiculo, String ds_placa, String ds_cor, int nr_ano) {
		super();
		this.cd_veiculo = cd_veiculo;
		this.ds_placa = ds_placa;
		this.ds_cor = ds_cor;
		this.nr_ano = nr_ano;
	}
	
	public int getCd_veiculo() {
		return cd_veiculo;
	}
	public void setCd_veiculo(int cd_veiculo) {
		this.cd_veiculo = cd_veiculo;
	}
	public String getDs_placa() {
		return ds_placa;
	}
	public void setDs_placa(String ds_placa) {
		this.ds_placa = ds_placa;
	}
	public String getDs_cor() {
		return ds_cor;
	}
	public void setDs_cor(String ds_cor) {
		this.ds_cor = ds_cor;
	}
	public int getNr_ano() {
		return nr_ano;
	}
	public void setNr_ano(int nr_ano) {
		this.nr_ano = nr_ano;
	}
	
	
	
}
