package br.com.fiap.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.dao.MotoristaDAO;
import br.com.fiap.entity.Motorista;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.InvalidCodeException;

public class MotoristaDAOImpl implements MotoristaDAO {
	
	private EntityManager em;

	public MotoristaDAOImpl() {
		super();
		this.em = em;
	}

	public Motorista busca(int nr_carteira) {
		return em.find(Motorista.class, nr_carteira);
	}

	public void cadastrar(Motorista moto) {
		em.persist(moto);
		
	}

	public void atualizar(Motorista moto) {
		em.merge(moto);
		
	}

	public void deletar(int nr_carteira) throws InvalidCodeException {
		Motorista buscaMotorista = busca(nr_carteira);
		em.remove(buscaMotorista);
		
	}

	public void commit() throws CommitException {

		try {
			
			em.getTransaction().begin();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new CommitException();
		}
	}

}
