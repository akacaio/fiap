package br.com.fiap.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table (name="T_MOTORISTA")
@SequenceGenerator(name = "seq_motorista", sequenceName="SQ_T_MOTORISTA", allocationSize=1)
public class Motorista {
	
	@Id
	@GeneratedValue(generator="seq_motorista", strategy= GenerationType.SEQUENCE)
	@Column(name="NR_CARTEIRA")
	private int nr_carteira;
	
	@Column(name="NM_MOTORISTA", nullable = false, length= 150)
	private String nm_motorista;
	
	@CreationTimestamp
	@Column(name="DT_NASCIMENTO")
	private Calendar dt_nascimento;
	
	@Lob
	@Column(name="FT_CARTEIRA")
	private byte[] ft_carteira;
	
	@Column(name="DS_GENERO", nullable = false)
	@Enumerated(EnumType.STRING)
	private TipoGenero ds_genero;

	public Motorista(int nr_carteira, String nm_motorista, Calendar dt_nascimento, byte[] ft_carteira,
			TipoGenero ds_genero) {
		super();
		this.nr_carteira = nr_carteira;
		this.nm_motorista = nm_motorista;
		this.dt_nascimento = dt_nascimento;
		this.ft_carteira = ft_carteira;
		this.ds_genero = ds_genero;
	}

	public Motorista(String nm_motorista, Calendar dt_nascimento, byte[] ft_carteira, TipoGenero ds_genero) {
		super();
		this.nm_motorista = nm_motorista;
		this.dt_nascimento = dt_nascimento;
		this.ft_carteira = ft_carteira;
		this.ds_genero = ds_genero;
	}

	public Motorista() {
		super();
	}

	public int getNr_carteira() {
		return nr_carteira;
	}

	public void setNr_carteira(int nr_carteira) {
		this.nr_carteira = nr_carteira;
	}

	public String getNm_motorista() {
		return nm_motorista;
	}

	public void setNm_motorista(String nm_motorista) {
		this.nm_motorista = nm_motorista;
	}

	public Calendar getDt_nascimento() {
		return dt_nascimento;
	}

	public void setDt_nascimento(Calendar dt_nascimento) {
		this.dt_nascimento = dt_nascimento;
	}

	public byte[] getFt_carteira() {
		return ft_carteira;
	}

	public void setFt_carteira(byte[] ft_carteira) {
		this.ft_carteira = ft_carteira;
	}

	public TipoGenero getDs_genero() {
		return ds_genero;
	}

	public void setDs_genero(TipoGenero ds_genero) {
		this.ds_genero = ds_genero;
	}

}
