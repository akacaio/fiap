package br.com.fiap.anotacao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD) // anota��o s� para atributos
@Retention(RetentionPolicy.RUNTIME) //
public @interface Coluna {
	
	String nome();
	String tipo();
	int tamanho() default 255;

}
