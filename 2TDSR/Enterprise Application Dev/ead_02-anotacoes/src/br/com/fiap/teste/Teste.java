package br.com.fiap.teste;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import br.com.fiap.anotacao.Coluna;
import br.com.fiap.bean.Pessoa;


public class Teste {
	
	public static void main(String[] args) {
		//Instaciar uma pessa
		Pessoa p = new Pessoa();
	
	//API Reflection -> obter o nome da classe
	String nome = p.getClass().getName();
	System.out.println("Nome da classe: " +nome);
	
	System.out.println("\nM�todos:");
	//Obter os m�todos da classe
	Method[] methods = p.getClass().getDeclaredMethods();
	for(Method item : methods) {
		System.out.println(item.getName());
	}
	
	System.out.println("\nAtributos:");
	//Obter todos os atributos da classe
	Field[] atributos = p.getClass().getDeclaredFields();
	for(int i =0; i < atributos.length; i++) {		
	System.out.println(atributos[i].getName());
	
	//Recuperar a anota��o @Coluna
	Coluna anotacao = atributos[i].getAnnotation(Coluna.class);
	System.out.println("Nome: " + anotacao.nome() +"\nTipo: " + anotacao.tipo() + "\nTamanho: " + anotacao.tamanho());
	  }
	
	
	}
}
