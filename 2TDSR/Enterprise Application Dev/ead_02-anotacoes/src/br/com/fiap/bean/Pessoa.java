package br.com.fiap.bean;

import java.util.Calendar;

import br.com.fiap.anotacao.Coluna;

public class Pessoa {
	
	@Coluna(nome="NM_CLNT", tipo="VARCHAR2")
	private String nome;
	
	@Coluna(nome="DT_NSCM", tipo="DATE")
	private Calendar dtNasc;
	
	@Coluna(nome="VL_PESO", tipo="NUMBER")
	private float peso;

	@SuppressWarnings("all") //utilizado somente como �ltimo recurso
	public void falar() {
		int idade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDtNasc() {
		return dtNasc;
	}

	public void setDtNasc(Calendar dtNasc) {
		this.dtNasc = dtNasc;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	
	
}
