package br.com.fiap.dao.impl;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;

import br.com.fiap.dao.GenericDAO;
import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.InvalidCodeException;

public abstract class GenericDAOImpl<T, K> implements GenericDAO<T, K> {

	private EntityManager em;
	private Class<T> clazz;

	public GenericDAOImpl() {
		super();
	}

	@SuppressWarnings("all")
	public GenericDAOImpl(EntityManager em) {
		super();
		this.em = em;
		this.clazz = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	@Override
	public void cadastrar(T entidade) {
		em.persist(entidade);

	}

	@Override
	public void alterar(T entidade) {
		em.merge(entidade);

	}

	@Override
	public void remover(K codigo) throws InvalidCodeException {
		T buscaCodigo = buscar(codigo);
		em.remove(codigo);

	}

	@Override
	public void commit() throws CommitException {

		try {
			em.getTransaction().begin();
			em.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			em.getTransaction().rollback();
			throw new CommitException();
		}

	}

	@Override
	public T buscar(K codigo) throws InvalidCodeException {
		T busca = em.find(clazz, codigo);
		if (busca == null) {
			throw new InvalidCodeException();
		}
		return busca;
	}

}
