package br.com.fiap.dao;

import br.com.fiap.exception.CommitException;
import br.com.fiap.exception.InvalidCodeException;

public interface GenericDAO<T, K> {

	void cadastrar(T entidade);

	void alterar(T entidade);

	void remover(K codigo) throws InvalidCodeException;

	void commit() throws CommitException;

	T buscar(K codigo) throws InvalidCodeException;

}
