package br.com.fiap.singleton;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactorySingleton {
	
	
	//1- atributo est�tico que ser� �nico
	private static EntityManagerFactory factory;
	
	//2- construtor vazio e privado
	private EntityManagerFactorySingleton() {}
	
	//3- m�todo est�tico que retorna a �nica instancia
	public static EntityManagerFactory getInstance() {
		if(factory == null) {
			factory = Persistence.createEntityManagerFactory("CLIENTE_ORACLE");
		}
		return factory;
	}	

}
