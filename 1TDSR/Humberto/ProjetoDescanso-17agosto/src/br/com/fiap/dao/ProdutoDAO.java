package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.conexao.Conexao;

public class ProdutoDAO {
	
	private Connection conexao;
	private PreparedStatement stmt;
	private ResultSet rs;
	
	public ProdutoDAO() throws Exception {
		conexao = Conexao.conectar();
	}
	
	public String cadastroProduto(ProdutoBeans produto) throws Exception {
		stmt = conexao.prepareStatement("INSERT INTO T_DDD_PRODUTO (CD_PRODUTO, DS_PRODUTO, VL_PRODUTO) VALUES (?,?,?)");
		
		stmt.setInt(1, produto.getCodigo());
		stmt.setString(2, produto.getDescricao());
		stmt.setInt(3, produto.getValor());
		stmt.executeUpdate();
		
		return "Dados gravados com sucesso!" +"\n"
				+ produto.getAll();
		
	}
	
		public List<ProdutoBeans> getMaior (int maiorValor) throws Exception {
		
		List<ProdutoBeans> valorMaior = new ArrayList<ProdutoBeans>();
		
		stmt = conexao.prepareStatement("SELECT * FROM T_DDD_PRODUTO WHERE VL_PRODUTO > ?");
		stmt.setInt(1, maiorValor);
		rs = null;
		rs = stmt.executeQuery();
		
		while(rs.next()) {
			valorMaior.add(
			new ProdutoBeans(
					rs.getInt("CD_PRODUTO"),
					rs.getString("DS_PRODUTO"),
					rs.getInt("VL_PRODUTO")));
		}
		return valorMaior;
	}
	
	public ProdutoBeans pesquisarCodigo(int consultaCodigo) throws Exception {
			
			stmt = conexao.prepareStatement("SELECT * FROM T_DDD_PRODUTO WHERE CD_PRODUTO = ?");
			
			stmt.setInt(1, consultaCodigo);
			rs = stmt.executeQuery();
			
			ProdutoBeans prod = new ProdutoBeans();
			
			if(rs.next()) {
				prod.setCodigo(rs.getInt("cd_produto"));
				prod.setDescricao(rs.getString("ds_produto"));
				prod.setValor(rs.getInt("vl_produto"));
			}
			
			return prod;
		}
		
	public String deletarProduto(int codigoProduto) throws Exception {
		
		stmt = conexao.prepareStatement("DELETE FROM T_DDD_PRODUTO WHERE CD_PRODUTO = ?");
		
		stmt.setInt(1, codigoProduto);
		stmt.executeUpdate();
		
		return "Dados deletados com sucesso";
		
	}
	
	public String depreciarProduto(int codigoProduto) throws Exception {
		
		stmt = conexao.prepareStatement("UPDATE T_DDD_PRODUTO SET VL_PRODUTO = VL_PRODUTO * 0.95 WHERE CD_PRODUTO = ?");
		
		stmt.setInt(1, codigoProduto);
		stmt.executeQuery();
		
		return "Valor alterado com sucesso.";
	}
	
	public String atualizaProduto(ProdutoBeans p) throws Exception {
		stmt = conexao.prepareStatement("UPDATE T_DDD_PRODUTO SET DS_PRODUTO = ?, VL_PRODUTO = ? WHERE CD_PRODUTO = ?");
		stmt.setInt(3, p.getCodigo());
		stmt.setString(1, p.getDescricao());
		stmt.setInt(2, p.getValor());
		
		stmt.executeQuery();
		
		return "Dados atualizados com sucesso.";
		
	}
	
	

}
