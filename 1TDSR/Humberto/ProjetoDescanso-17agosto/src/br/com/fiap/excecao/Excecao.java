package br.com.fiap.excecao;

public class Excecao extends Exception{
	
	public static String tratarExcecao(Exception e) {
		//Chama a Classe. o nome.comparar string e o erro;
		if(e.getClass().getName().equals("java.lang.NumberFormatException")) {
			return "N�mero inv�lido.";
		}else if(e.getClass().getName().equals("java.sql.SQLException")){
			return "Erro ao conectar no banco.";
		}else {
			return "Ocorreu uma falha.";
		}
		
	}
	
}