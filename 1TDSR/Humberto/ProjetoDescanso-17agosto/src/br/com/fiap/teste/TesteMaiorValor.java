package br.com.fiap.teste;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.dao.ProdutoDAO;
import br.com.fiap.excecao.Excecao;

public class TesteMaiorValor {
	
	public static void main(String[] args) {
	
	try {
		
		ProdutoDAO dao = new ProdutoDAO();
		List<ProdutoBeans> listaProduto = new ArrayList<>();
		
		do {
			
			listaProduto = dao.getMaior(Integer.parseInt(JOptionPane.showInputDialog("Insira o valor: ")));
			
			for(ProdutoBeans p : listaProduto) {
				System.out.println("C�digo " + p.getCodigo());
				System.out.println("Descri��o " + p.getDescricao());
				System.out.println("Valor: " + p.getValor());
			}
			
		} while ((JOptionPane.showConfirmDialog
				(null,
				"Continuar",
				"T�tulo",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE)==0));
		
		
	} catch (Exception e) {
		e.printStackTrace();
		System.out.println(Excecao.tratarExcecao(e));
	}
}}
