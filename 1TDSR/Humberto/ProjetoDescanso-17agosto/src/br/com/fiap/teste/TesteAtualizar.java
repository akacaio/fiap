package br.com.fiap.teste;

import javax.swing.JOptionPane;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.dao.ProdutoDAO;
import br.com.fiap.excecao.Excecao;

public class TesteAtualizar {
	
	public static void main(String[] args) {
		try {
			
			ProdutoBeans beans = new ProdutoBeans();
			ProdutoDAO dao = new ProdutoDAO();
			
			beans.setCodigo(Integer.parseInt(JOptionPane.showInputDialog("Digite o C�digo: ")));
			beans.setDescricao(JOptionPane.showInputDialog("Nova descri��o do produto: "));
			beans.setValor(Integer.parseInt(JOptionPane.showInputDialog("Insira o novo valor: ")));
			
			System.out.println(dao.atualizaProduto(beans));
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Excecao.tratarExcecao(e));
		}
		
	}

}
