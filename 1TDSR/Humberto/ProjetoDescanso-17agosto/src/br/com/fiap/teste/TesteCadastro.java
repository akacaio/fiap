package br.com.fiap.teste;

import javax.swing.JOptionPane;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.dao.ProdutoDAO;
import br.com.fiap.excecao.Excecao;

public class TesteCadastro {
	
	public static void main(String[] args) {
		
		try {
			
			ProdutoBeans produto = new ProdutoBeans();
			ProdutoDAO dao = new ProdutoDAO();
			
			produto.setCodigo(Integer.parseInt(JOptionPane.showInputDialog("C�digo:")));
			produto.setDescricao(JOptionPane.showInputDialog("Descri��o: "));
			produto.setValor(Integer.parseInt(JOptionPane.showInputDialog("Valor: ")));
			
			System.out.println(dao.cadastroProduto(produto));
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Excecao.tratarExcecao(e));
		}
	}

}
