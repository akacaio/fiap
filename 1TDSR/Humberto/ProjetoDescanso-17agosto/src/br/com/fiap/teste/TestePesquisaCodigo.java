package br.com.fiap.teste;

import javax.swing.JOptionPane;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.dao.ProdutoDAO;
import br.com.fiap.excecao.Excecao;

public class TestePesquisaCodigo {
	
	public static void main(String[] args) {
		
		try {
			
			ProdutoDAO dao = new ProdutoDAO();
			ProdutoBeans produto = new ProdutoBeans();
			
			produto = dao.pesquisarCodigo(
					Integer.parseInt(
					JOptionPane.showInputDialog(
					"C�digo do Produto a ser encontrado: ")));
			
			System.out.println(produto.getAll());
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Excecao.tratarExcecao(e));
		}
		
		
		
	}

}
