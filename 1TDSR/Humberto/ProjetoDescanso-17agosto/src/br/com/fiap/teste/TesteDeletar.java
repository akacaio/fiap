package br.com.fiap.teste;

import javax.swing.JOptionPane;

import br.com.fiap.beans.ProdutoBeans;
import br.com.fiap.dao.ProdutoDAO;
import br.com.fiap.excecao.Excecao;

public class TesteDeletar {
	
	public static void main(String[] args) {
		
		try {
			
			ProdutoDAO dao = new ProdutoDAO();
			
			System.out.println(dao.deletarProduto(
					Integer.parseInt(
							JOptionPane.
							showInputDialog(
									"Digite o ID para deletar: "))));
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Excecao.tratarExcecao(e));
		}
	}

}
