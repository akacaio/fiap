package br.com.fiap.beans;

public class ProdutoBeans {
	
	private int cd_produto;
	private String ds_produto;
	private int vl_produto;
	
	public ProdutoBeans() {}
	
	public ProdutoBeans(int cd_produto, String ds_produto, int vl_produto) {
		setCodigo(cd_produto);
		setDescricao(ds_produto);
		setValor(vl_produto);
	}
	
	public String getAll() {
		return "C�digo: "+ cd_produto + "\n"+ "Descri��o: " + ds_produto + "\n" + "Valor: "+ vl_produto;
	}
	
	public int getCodigo() {
		return cd_produto;
	}
	
	public void setCodigo(int cd_produto) {
		this.cd_produto = cd_produto;
	}
	
	public String getDescricao() {
		return ds_produto;
	}
	
	public void setDescricao(String ds_produto) {
		this.ds_produto = ds_produto;
	}
	
	public int getValor() {
		return vl_produto;
	}
	
	public void setValor(int vl_produto) {
		this.vl_produto = vl_produto;
	}

}
