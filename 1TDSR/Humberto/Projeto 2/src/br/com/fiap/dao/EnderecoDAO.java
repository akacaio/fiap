package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.com.fiap.beans.Endereco;
import br.com.fiap.conexao.Conexao;

public class EnderecoDAO {
	
	private Connection con;
	private PreparedStatement stmt;
	private ResultSet rs;
	
	public EnderecoDAO() throws Exception {
		con = Conexao.conectar();
	}

	public String gravarEndereco(Endereco end) throws Exception {
		
		stmt = con.prepareStatement("INSERT INTO T_DDD_ENDERECO (CD_ENDERECO, DS_LOGRADOURO, NR_ENDERECO, NR_CEP) VALUES (?,?,?,?)");
		
		stmt.setInt(1, end.getCodigoEndereco());
		stmt.setString(2, end.getLogradouro());
		stmt.setString(3, end.getNumero());
		stmt.setString(4, end.getCep());
		stmt.executeUpdate();
		
		return "Gravado com sucesso";
		
	}
	
	public Endereco consultarPorCodigo(int codigo) throws Exception {
		
		stmt = con.prepareStatement("SELECT * FROM T_DDD_ENDERECO WHERE CD_ENDERECO = ?");
		
		stmt.setInt(1, codigo);
		rs = stmt.executeQuery();
		
		if(rs.next()) {
			return new Endereco (
			rs.getInt("CD_ENDERECO"),
			rs.getString("DS_LOGRADOURO"),
			rs.getString("NR_ENDERECO"),
			rs.getString("NR_CEP")
			);
		} else {
		return new Endereco();
	  }
	}
	
	public void fecharConexao() throws Exception {
		con.close();
	}
	
}
