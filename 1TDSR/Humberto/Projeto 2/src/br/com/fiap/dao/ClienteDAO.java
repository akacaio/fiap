package br.com.fiap.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.beans.Cliente;
import br.com.fiap.beans.Endereco;
import br.com.fiap.conexao.Conexao;

/**
 * Classe respons�vel por manipular a tabela T_DDD_CLIENTE
 * @author Caio Duarte
 * 
 */

public class ClienteDAO {
	
	private Connection con;
	private PreparedStatement stmt;
	private ResultSet rs;
	
	/**
	 * Construtor respons�vel por abrir a conex�o
	 * @throws Exception ExceC�o checked SQLException
	 * @author Caio Duarte
	 */
	
	public ClienteDAO() throws Exception {
		con = Conexao.conectar();
	}
	
	/**
	 * Adiciona uma tupla na tabela T_DDD_CLIENTE
	 * @param obj Recebe um objeto ClienteBeans
	 * @return Retorna uma String de operaC�o com sucesso
	 * @throws Exception ExceC�o checked SQLException
	 * @author Caio Duarte
	 */
	
	public String gravar(Cliente obj) throws Exception {
		stmt = con.prepareStatement("INSERT INTO T_DDD_CLIENTE(NM_CLIENTE, NR_CLIENTE, QT_ESTRELAS, FK_ENDERECO) VALUES (?,?,?,?)");
		
		stmt.setString(1, obj.getNome());
		stmt.setInt(2, obj.getNumero());
		stmt.setInt(3, obj.getQuantidadeEstrelas());
		stmt.setInt(4, obj.getEndereco().getCodigoEndereco());
		stmt.executeUpdate();
		
		return "Gravado com sucesso.";
	}
	
	public Cliente pesquisarNumero(int consultaNumero) throws Exception {
		stmt = 	con.prepareStatement("SELECT * FROM T_DDD_CLIENTE INNER JOIN T_DDD_ENDERECO ON (T_DDD_ENDERECO.CD_ENDERECO = T_DDD_CLIENTE.FK_ENDERECO) WHERE NR_CLIENTE =?");
		
		stmt.setInt(1, consultaNumero);
		
		rs = stmt.executeQuery();
		
		Cliente cliente = new Cliente();
		
		if(rs.next()){
		cliente.setNome(rs.getString("NM_CLIENTE"));
		cliente.setNumero(rs.getInt("NR_CLIENTE"));
		cliente.setQuantidadeEstrelas(rs.getInt("QT_ESTRELAS"));
		cliente.setEndereco(new Endereco(
				rs.getInt("CD_ENDERECO"),
				rs.getString("DS_LOGRADOURO"),
				rs.getString("NR_ENDERECO"),
				rs.getString("NR_CEP")
				));
		}
		return cliente;
}
	
	public List<Cliente> pesquisarPorNome(String nome) throws Exception{
		
		List<Cliente> lista = new ArrayList<Cliente>();
		
		stmt = con.prepareStatement
				("SELECT * FROM T_DDD_CLIENTE INNER JOIN T_DDD_ENDERECO ON (T_DDD_ENDERECO.CD_ENDERECO = T_DDD_CLIENTE.FK_ENDERECO) WHERE NM_CLIENTE LIKE ?");
		stmt.setString(1, "%" + nome + "%");
		rs = stmt.executeQuery();
		while(rs.next()) {
			lista.add(new Cliente(
					rs.getString("NM_CLIENTE"), 
					rs.getInt("NR_CLIENTE"),
					rs.getInt("QT_ESTRELAS"),
					new Endereco(
					rs.getInt("CD_ENDERECO"),
					rs.getString("DS_LOGRADOURO"),
					rs.getString("NR_ENDERECO"),
					rs.getString("NR_CEP")
							)
					));
		}
		return lista;
	}
	
	public int apagarCliente(int numero) throws Exception{
		stmt = con.prepareStatement("DELETE FROM T_DDD_CLIENTE WHERE NR_CLIENTE = ?");
		stmt.setInt(1, numero);
		return stmt.executeUpdate();	
	}
	
	public int promoverCliente(int numero) throws Exception{
		stmt = con.prepareStatement("UPDATE T_DDD_CLIENTE SET QT_ESTRELAS = QT_ESTRELAS + 1 WHERE NR_CLIENTE = ?");
		stmt.setInt(1, numero);
		return stmt.executeUpdate();		
	}
	
	public int despromoverCliente(int numero) throws Exception{
		stmt = con.prepareStatement("UPDATE T_DDD_CLIENTE SET QT_ESTRELAS= QT_ESTRELAS -1 WHERE NR_CLIENTE = ?");
		stmt.setInt(1, numero);
		return stmt.executeUpdate();
	}
	
	public void fecharConexao() throws Exception {
		con.close();
	}
	
}
