package br.com.fiap.teste;

import javax.swing.JOptionPane;

import br.com.fiap.beans.Cliente;
import br.com.fiap.beans.Endereco;
import br.com.fiap.bo.ClienteBO;
import br.com.fiap.excecao.Excecao;

public class TesteNovoCliente {

	public static void main(String[] args) {

		try {
			
			System.out.println(ClienteBO.novoCliente(new Cliente(
					JOptionPane.showInputDialog("Digite o nome: "),
					Integer.parseInt(JOptionPane.showInputDialog("Digite o n�mero: ")),
					Integer.parseInt(JOptionPane.showInputDialog("Estrelas: ")),
							new Endereco(
							Integer.parseInt(JOptionPane.showInputDialog("Digite o C�digo do Endere�o: ")),
							JOptionPane.showInputDialog("Digite o Logradouro"),
							JOptionPane.showInputDialog("Digite o n�mero do logradouro: "),
							JOptionPane.showInputDialog("Digite o CEP: ")
							))));
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(Excecao.tratarExcecao(e));
		}

	}

}
