package br.com.fiap.bo;

import br.com.fiap.beans.Cliente;
import br.com.fiap.dao.ClienteDAO;


/**
 * 
 * Respons�vel por todas as regras de neg�cio e padroniza��es do Cliente
 * 1) O nome n�o pode conter mais que 40 caracteres
 * 2) A quantidade de estrelas deve estar entre 1 e 5
 * 3) O n�mero do cliente n�o pode ser duplicado
 * @author Caio Duarte
 * @author Guilherme Caruso
 * @author Joana Moraes
 * @author Adenilton
 * @author Victor Mascher
 * 
 * @version 1.0
 * @since 1.0
 * @see br.com.fiap.dao.ClienteDAO // quais as classes que s�o interligadas com o documento do BO
 * @see br.com.fiap.beans.Cliente
 * 
 */



public class ClienteBO {
	
	/**
	 * Este m�todo ir� validar e repassar o objeto cliente para a classe ClienteDAO
	 * @param c - Este parametro representa um objeto do Beans.
	 * @return O m�todo retorna uma string com mensagem de sucesso
	 * @throws Exception Tratando a exce��o checked SQLException
	 * @author Caio Duarte
	 */
	
	public static String novoCliente(Cliente c) throws Exception {
		
		if(c.getEndereco() == null) {
			return "Endere�o inv�lido";
		}
		
		//valida��o
		if(c.getNome().length() > 40) {
			return "Nome inv�lido.";
		}
		// regra de neg�cio
		if(c.getQuantidadeEstrelas() < 1 || c.getQuantidadeEstrelas() > 5) {
			return "Quantidade de estrelas inv�lida.";
		}
		// padroniza��o
		c.setNome(c.getNome().toUpperCase());
		
		ClienteDAO dao = new ClienteDAO();
		
		Cliente exists = dao.pesquisarNumero(c.getNumero());
		
		if(exists.getNumero() > 0) {
			dao.fecharConexao();
			return "Cliente j� cadastrado.";
		}
		
		String resp = EnderecoBO.novoEndereco(c.getEndereco());
		
		if(!resp.equals("Endere�o gravado com sucesso.")) {
			return resp;
		}
		
		String dados = dao.gravar(c);
		dao.fecharConexao();

		return dados;
	}
	
	
}