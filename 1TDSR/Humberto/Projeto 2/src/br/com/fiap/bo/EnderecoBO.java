package br.com.fiap.bo;

import br.com.fiap.beans.Endereco;
import br.com.fiap.dao.EnderecoDAO;

public class EnderecoBO {
	
	public static String novoEndereco(Endereco e) throws Exception {

		if(e.getCep().length() != 9) {
			return "CEP inv�lido";
		}
		
		if(e.getLogradouro().length() > 50) {
			return "Logradouro inv�lido";
		}
		
		e.setLogradouro(e.getLogradouro().toUpperCase());
		e.setNumero(e.getNumero().toUpperCase());
		
		EnderecoDAO daoEnd = new EnderecoDAO();
		Endereco ende = daoEnd.consultarPorCodigo(e.getCodigoEndereco());
		
		if(ende.getCodigoEndereco() == 0) {
			daoEnd.gravarEndereco(e);
		}
		
		daoEnd.fecharConexao();
		return "Endere�o gravado com sucesso.";
	}

}
