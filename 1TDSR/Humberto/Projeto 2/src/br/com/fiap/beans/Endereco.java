package br.com.fiap.beans;

public class Endereco {
	
	private int codigoEndereco;
	private String logradouro;
	private String numero;
	private String cep;
	
	public Endereco() {}
	
	public Endereco(int codigoEndereco, String logradouro, String numero, String cep) {
	setCodigoEndereco(codigoEndereco);
	setLogradouro(logradouro);
	setNumero(numero);
	setCep(cep);
	}

	public String getAll() {
		return codigoEndereco + logradouro + numero + cep;
	}
	
	public void setAll(int codigoEndereco, String logradouro, String numero, String cep) {
		setCodigoEndereco(codigoEndereco);
		setLogradouro(logradouro);
		setNumero(numero);
		setCep(cep);
	}
	
	public int getCodigoEndereco() {
		return codigoEndereco;
	}

	public void setCodigoEndereco(int codigoEndereco) {
		this.codigoEndereco = codigoEndereco;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}
	
	

}
