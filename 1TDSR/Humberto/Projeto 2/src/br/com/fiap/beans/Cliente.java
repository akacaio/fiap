package br.com.fiap.beans;

public class Cliente {
	
	private String nome;
	private int numero;
	private int quantidadeEstrelas;
	private Endereco endereco;
	
	

	public Cliente() {
		super();
	}

	public Cliente(String nome, int numero, int quantidadeEstrelas, Endereco endereco) {
		super();
		this.nome = nome;
		this.numero = numero;
		this.quantidadeEstrelas = quantidadeEstrelas;
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public int getQuantidadeEstrelas() {
		return quantidadeEstrelas;
	}
	
	public void setQuantidadeEstrelas(int quantidadeEstrelas) {
		this.quantidadeEstrelas = quantidadeEstrelas;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
