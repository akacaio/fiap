package br.com.fiap.teste;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.fiap.beans.Cargo;

public class TesteLista {

	public static void main(String[] args) {
		List<Cargo> cargos = new ArrayList();
		
		cargos.add(new Cargo("Dev", "JUNIOR", 2000));
		cargos.add(new Cargo("DBA", "JUNIOR", 3000));
		cargos.add(new Cargo("Frontend Dev", "JUNIOR", 1000));
		cargos.add(new Cargo("Analista", "PLENO", 10000));
		
		Collections.sort(cargos);
		
		for(Cargo c : cargos) {
			System.out.println(c.getNome());
			System.out.println(c.getNivel());
			System.out.println(c.getSalario());
			System.out.println("---");
		}
	}
}