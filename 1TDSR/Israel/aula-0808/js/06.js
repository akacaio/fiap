$(function () {
  // localizar e selecionar somente o primeiro elemento
  $('#lista1 li:first').css('border', '2px solid #369');

  // localizar e selecionar somente o último elemento
  $('#lista1 li:last').css('border', '2px solid #369');

  // todos menos o último elemento da lista
  $('#lista2 li:not(:last)').css('border','2px solid #369');

  //selecionar todos os elementos ímpares
  $('#lista3 li:odd').css('border','2px solid #369');

  //selecionar todos os elementos pares
  $('#lista4 li:even').css('border','2px solid #369');

  // selecionar partindo de uma posição do array
  $('#lista5 li:gt(1)').css('border','2px solid #369');

  // selecionar um elemento anterior
  $('#item').prev().css('border','2px solid #369');

  // selecionar um elemento posterior
  $('#item').next().css('border','2px solid #369');

  // selecionar todos os elemtnso dentro de um principal pai)
  $('#lista7').children().css('border','2px solid #369');

  // localizar e selecionar todos os elementos
  $('body').find('span').css('background','#DC143C');
});
