$(function() {
  //click() - gera o evento quando se produz um clique em algum elemento da pagina
  $('#first').click(function () {
      $(this).css('color','orange').css('background','#000');
                             });

  //dblclick - gera um evento após o duplo clique

  $('#dois').dblclick(function () {
    $(this).css('background','#003').css('color','#fff');
  });


  //hover - ele manipula 2 eventos
  // quando o mouse fica acima do Elemento
  // quando o mouse sai do Elemento
  // é preciso passar 2 funções

  $('#tres').hover(function () {
    $(this).css('background','orange').css('color','#fff')},
    function () {
    $(this).css('background', '#fff').css('color','#000');
  });

  //mouseup - cria um evento assim que o usuário clicar e soltar o click
  $('#quatro').mouseup(function () {
    $(this).css('font-family', 'Calibri').css('font-size','18px').css('color','red');
  });

  //mouseout cria um evento quando o usuário tira o mouse do elemento
  $('#cinco').mouseout(function () {
    $(this).css('background','purple');
  });

  //mousemove - evento que é exectado assim que movemos o mouseup
  $(document).mousemove(function (e) {
    $('#move').text('Posição do eixo X= ' +e.pageX + ' Posição do eixo Y= ' +e.pageY);
  })
});
