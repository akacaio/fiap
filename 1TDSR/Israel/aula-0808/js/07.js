$(function () {
  //append - insere um elemento no final do indicado
  $('#um').append('<p>Inserido pelo Append</p>');

  //prepend - insere um elemento no início do indicado
  $('#dois').prepend('<h2>Inserido pelo prepend</h2>');

  //html() - substituir conteúdo Selecionando
  $('#tres').html('<h1>Agora será um h1</h1>')

  $('h1').text('Agora é outro texto.');

  //appen com apenas conteúdo
  $('h1').append(' , FOMEEEEEEEEEEEEEE!');

  // prepend com apenas conteúdo
  $('h1').prepend(' , FOMEEEEEEEEEEEEEE!');

  //localizar um conteúdo específico
  $('h3:contains("sumir")').css('background', '#ddc');

  //inserir um elemento antes de outro - insertBefore()
  $('<a href="www.google.com.br">Google</a>').insertBefore('article').css('text-decoration','none');

  //inserir outro elemento depois de outro - insertAfter()
  $('<p>Olá meu querido, vai se foder!</p>').css('background', 'red').insertAfter('article');

  //empty() - limpar um conteúdo
  $('span').empty();

  //rempve - remover um Elemento
  $('#some').remove();

  //replaceAll() - substituir um elemento por outro
  $('<p>Ola cuzao</p>').replaceAll('#troca');
});
