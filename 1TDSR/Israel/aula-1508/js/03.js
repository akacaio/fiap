/*regra da senha
 vai de zero a 100
 deve ter letras - 25
 deve ter numeros - 25
 deve ter caracteres especiais - 25
 minimo de 10 caracteres - 25
*/

$(function () {
  $('#senha').keyup(function () {
    var pass = $(this).val();
    var forca = 0;

    if(pass.length >= 10){ forca += 25; }

    // verificar se tem letras
    var hasChar = new RegExp(/[a-z]/i);
    if(hasChar.test(pass)){ forca += 25; }

    // verificar se tem numero
    var hasNum = new RegExp(/[0-9]/);
    if(hasNum.test(pass)){ forca += 25; }

    //caracteres especiais
    var especChar = new RegExp(/[^a-z0-9]/i);
    if(especChar.test(pass)) { forca += 25; }

    if(forca >= 75){
      var status = "Senha aceita.";
    } else {
      status = "Senha não aceita";
    }

    $('#forca').text("Força: "+ forca +" (" + pass + ") " + status);

  });
});
