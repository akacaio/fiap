$(function() {

  $('#botao1').click(function() {
    $('#um').toggleClass('formata');
  });

  // quando o elemento ganha foco
  $('#login,#senha').focus(function() {
      $(this).addClass('focado');
    });

  //quando o elemento perde o foco
  $('#login,#senha').blur(function() {
    $(this).removeClass('focado');
  });

  //tecla pressionada
  $('#login').keydown(function() {
    console.log("Alguma tecla foi pressionada.");
  });

  // soltar a tecla
  $('#login').keyup(function() {
    console.log("Alguma tecla foi solta.");
  });

  //mostrar o número da tecla que foi digitada
  $('#login').keydown(function(e){
    console.log(e.keyCode);
  });

  //aparecer / desaparecer elemento
  $('#btn2').click(function() {
    $('#dois').toggle('slow');
  });

  //Fade
  $('#btn3').click(function () {
    $('#dois').fadeToggle('slow').css({
      'background':'#574'
    });
    });

    $('#btn4').click(function () {
      $('#dois').slideToggle('slow');
    });

      // avançar
    $('#btn5').click(function () {
      $('#dois').animate({
        'margin-left':'+=50px'},'slow');
    });

    //voltar
    $('#btn6').click(function () {
      $('#dois').animate({
        'margin-left':'-=50px'},'slow');
    });

    //animate
    $('#btn7').click(function () {
      $('#dois').animate({
        'margin-left': 100,
        'margin-top': 120,
        'border-radius': 50
        }, 1500);
    });

    //ir e Voltar
    $('#btn8').click(function () {
      $('#dois').animate({
        'margin-left': 100,
        'margin-top': 120,
        'border-radius': 50 },
      {
        duration: 1500,
        complete:function () {
          $('#dois').animate({
            'margin-left': 0,
            'margin-top': 20,
            'border-radius': 0 }
            , 1500);
          }
      });
  });

});
