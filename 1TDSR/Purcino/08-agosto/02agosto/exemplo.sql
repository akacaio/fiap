CREATE TABLE T_A14_AUTOR(
    --n�o pode se criar chave composta como abaixo
    cd_autor INTEGER PRIMARY KEY,
    nm_autor VARCHAR2(50) NOT NULL
);

CREATE TABLE T_A14_LIVRARIA(
    cd_livraria INTEGER,
    nm_livraria VARCHAR2(100) NOT NULL,
    --outra maneira de se fazer a chave primaria abaixo--
    PRIMARY KEY (cd_livraria)
);

CREATE TABLE T_A14_LIVRO(
    cd_livro INTEGER,
    tx_titulo VARCHAR2(200)NOT NULL,
    nr_paginas INTEGER NOT NULL,
    cd_autor INTEGER NOT NULL,
    PRIMARY KEY (cd_livro),
    FOREIGN KEY(cd_autor) REFERENCES T_A14_AUTOR(cd_autor)
);

CREATE TABLE T_A14_LIVRO_LIVRARIA(
    cd_livro INTEGER,
    cd_livraria INTEGER,
    -- chave primaria composta abaixo exemplo de como criar 
    -- chave primaria sempre sera uma s� se tiverem duas ser�o na mesma PRIMARY KEY
    PRIMARY KEY (cd_livro, cd_livraria),
    FOREIGN KEY (cd_livro) REFERENCES T_A14_LIVRO(cd_livro),
    FOREIGN KEY (cd_livraria) REFERENCES T_A14_LIVRARIA(cd_livraria)
);