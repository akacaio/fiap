import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class AlgoritmoBusca {
	
	double[] vetor = new double[10];
	
	public double buscaVetor(double[] vet, double position) {
		for (int i = 0; i < vet.length; i++) {
			if(vet[i] == position) return i;
		}	
		return -1;
	}
	
	public int busca(List<String> colecao, String x) {
		
		for (int i = 0; i < colecao.size(); i++) {
			if(colecao.get(i).equals(x)) {
				return i;
			}
		}
		return -1;
	}
	

	public List<String> converteArquivoTXT(String arquivo) throws Exception {
		
		FileReader fr = new FileReader(arquivo);
		BufferedReader br = new BufferedReader(fr);
		
		List<String> listaRetorno = new ArrayList<String>();
		
		String linha = br.readLine();
		while(linha != null) {
			listaRetorno.add(linha);
			linha = br.readLine();
		}
		br.close();		
		return listaRetorno;
	}
	
	public static void main(String[] args) throws Exception {
		
		AlgoritmoBusca aB = new AlgoritmoBusca();
		
		List<String> listaA = aB.converteArquivoTXT("D:/lista454298.csv");
		List<String> listaB = aB.converteArquivoTXT("D:/lista25403.csv");
		
		for (int i = 0; i < listaB.size(); i++) {
			String s = listaB.get(i);
			int r = aB.busca(listaA, s);
			if(r == -1) {
			System.out.println(s);
			}
		}
	}

}
