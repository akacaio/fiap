package br.com.fiap.gondo;

import java.io.BufferedReader;
import java.io.FileReader;

public class Cacapalavras {
	
	public static void main(String[] args) throws Exception{
		
		FileReader fr = new FileReader("C:\\Users\\logonrmlocal\\Desktop\\cacapalavra.txt");
		BufferedReader br = new BufferedReader(fr);
		
		String linha = br.readLine();
		
		int dim = Integer.parseInt(linha);
		char[][] diagrama = new char[dim+2][dim+2];
		preencheDiagrama(diagrama);
		
		for (int i = 0; i < dim; i++) {
			linha = br.readLine();
			preencherLinhaMatriz(i, diagrama, linha);
		}
		
		linha = br.readLine();
		int qtdePalavras = Integer.parseInt(linha);
		String[] lista = new String[qtdePalavras];
		
		for (int i = 0; i < qtdePalavras; i++) {
			lista[i] = br.readLine();
		}
		
		for (int i = 0; i < qtdePalavras; i++) {
			System.out.println("Procurando: " + lista[i]);
			procuraPalavra(lista[i], diagrama);
		}
	}
	
	static void procuraPalavra(String pal, char[][] mat){
		boolean resp = false;
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				resp = palavraLeste(pal, mat, i, j);
				if(resp==true) {
					System.out.println("A Leste" + "\n" + "Linha:" + i + "\n" + "Coluna:" + j);
				}
				
			}
		}
	}
	
	
	
	
	
	private static boolean palavraLeste(String pal, char[][] mat, int lin, int col) {
				
			int k =0;
			while(k < pal.length() && mat[lin][col] == pal.charAt(k)) {
				
				k++;
				col++;
			}
			
			if(k == pal.length()) {
				
				return true;
		}else {
			return false;
		}
	}
	

	private static void preencherLinhaMatriz(int lin, char[][] mat, String conjunto) {
		for (int col = 0; col < conjunto.length(); col++) {
			mat[lin][col+1]=conjunto.charAt(col);
		}
		
	}

	private static void preencheDiagrama(char[][] mat) {
		for (int i = 0; i < mat.length; i++) {
			for (int j = 0; j < mat[0].length; j++) {
				mat[i][j] = '@';
			}
		}
		
	}
}
