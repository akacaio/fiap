package br.com.fiap.gondo;

import br.com.fiap.imagens.BibliotecaImagens;

public class Exercicio13 {
	
	public static void main(String[] args) {
	
	BibliotecaImagens bi =	new BibliotecaImagens("D:/foto.jpg");
	
	int[][] red = bi.getRed();
	int[][] green = bi.getGreen();
	int[][] blue = bi.getBlue();
	
	int lin = red.length;
	int col = red[0].length;

	int[][] cinza = new int[lin][col];
	
	for (int i = 0; i < cinza.length; i++) {
		for (int j = 0; j < cinza[i].length; j++) {
			double c = 0.3 * red[i][j] + 0.59 * green[i][j] + 0.11 * blue[i][j];
			cinza[i][j] = (int) c;
		}
	}
	
	bi.gravaArquivoTonsCinza("D:/fotoCinza.jpg", cinza);
	
	}
}
