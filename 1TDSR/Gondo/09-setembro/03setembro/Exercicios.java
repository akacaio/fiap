package br.com.fiap.gondo;

import java.util.Random;

public class Exercicios {
	
	public int somaPos(int m[][]) {
		
		int acumulador = 0;
		
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				acumulador = acumulador + m[i][j];
			}
		}
		
		return acumulador;
		
	}
	
	public static void main(String[] args) {
		
		int[][] matriz = new int [5][7];
		int valor;
		
		Random rand = new Random();
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[i].length; j++) {
				valor = rand.nextInt(1001); // gera n�meros aleat�rios de 0 at� 1000
				matriz[i][j] = valor;
				System.out.print(matriz[i][j] + "\t");
			}
			System.out.println();
		}		
	}

}
