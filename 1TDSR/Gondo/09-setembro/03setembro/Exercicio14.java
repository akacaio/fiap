package br.com.fiap.gondo;

import br.com.fiap.imagens.BibliotecaImagens;

public class Exercicio14 {
	
	public static void main(String[] args) {
		
		BibliotecaImagens bi = new BibliotecaImagens("D:/lago_canada.jpg");
		
		
		int[][] red = bi.getRed();
		int[][] green = bi.getGreen();
		int[][] blue = bi.getBlue();
		
		int lin = red.length;
		int col = red[0].length;
		
		int[][] r = new int[lin][col];
		int[][] g = new int[lin][col];
		int[][] b = new int[lin][col];
		
		for (int i = 0; i < lin; i++) {
			for (int j = 0; j < col; j++) {
				r[i][col -1 -j] = red[i][j];
				g[i][col -1 -j] = green[i][j];
				b[i][col -1 -j] = blue[i][j];
			}
			
		}
		
		bi.gravaArquivoColorido("D:/lagoinvertido.png", r, g, b);

		
	}

}
