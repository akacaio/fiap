package br.com.fiap.gondo;

public class Velha {
	
	public static final char BRANCO = ' ';
	
	private char[][] tabuleiro;
	private int jogadas;
	
	public Velha() { // construtor serve para instanciar o objeto e iniciar atributos
		tabuleiro = new char[3][3];
		jogadas = 0;
		
		for (int i = 0; i < tabuleiro.length; i++) {
			for (int j = 0; j < tabuleiro[i].length; j++) {
				tabuleiro[i][j] = BRANCO;
			}
		}
	}

	public boolean temEspaco() {
		
		if(jogadas <= 9) {
			return true;
		} else
			{
			return false;
		}
	}

	public boolean haGanhador() {
		
	for (int i = 0; i < 3; i++) {	
		if(tabuleiro[i][0] != BRANCO &&
		   tabuleiro[i][0] == tabuleiro [i][1] &&
		   tabuleiro[i][1] == tabuleiro[i][2] ) {
			return true;
			}
		}
	
	for (int j = 0; j < 3; j++) {	
		if(tabuleiro[0][j] != BRANCO &&
		   tabuleiro[0][j] == tabuleiro [1][j] &&
		   tabuleiro[1][j] == tabuleiro[2][j] ) {
			return true;
			}
		}
		
		if(tabuleiro[0][0] != BRANCO &&
		   tabuleiro[0][0] == tabuleiro [1][1] &&
		   tabuleiro[1][1] == tabuleiro[2][2] ) {
			return true;
		}
	
		if(tabuleiro[0][2] != BRANCO &&
			   tabuleiro[0][2] == tabuleiro [1][1] &&
			   tabuleiro[1][1] == tabuleiro[2][0] ) {
				return true;
				}
	
		return false;
	}	

	public boolean joga(int linha, int coluna, char jogador) {
		
		if(tabuleiro[linha][coluna] == BRANCO) {
			tabuleiro[linha][coluna] = jogador;
			jogadas++;
			return true;
		}
		return false;
	}

	public char trocaJogador(char jogador) {

		if(jogador == 'X') {
			return 'O';
		} else {
			return 'X';
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tabuleiro.length; i++) {
			for (int j = 0; j < tabuleiro[i].length; j++) {
				sb.append(tabuleiro[i][j]);
				sb.append("\t");
			}
			sb.append("\n");
		}
		return sb.toString();
	}

}
