package br.com.fiap.gondo;

public class Exercicio6 {
	
	public int[] busca(double[][] mat, double x) {
		
		int[] retorno = new int[2];
		
	for (int i = 0; i < mat.length; i++) {
		for (int j = 0; j < mat[i].length; j++) {
			if(mat[i][j] == x) {
				retorno[0] = i;
				retorno[1] = j;
				return retorno;
			}
		
		}
	}
	
	return null;
}
	
	public static void main(String[] args) {
		
	}

}
