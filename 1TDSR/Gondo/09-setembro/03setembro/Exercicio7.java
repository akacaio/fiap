package br.com.fiap.gondo;

import java.util.Scanner;

public class Exercicio7 {

	public static void main(String args[]) {
		Velha v = new Velha();
		char jogador = 'X';
		Scanner entrada = new Scanner(System.in);
		while (v.temEspaco() && !v.haGanhador()) {
			System.out.println("Vez do jogador " + jogador);
			System.out.print(" Linha : ");
			int linha = entrada.nextInt();
			System.out.print(" Coluna : ");
			int coluna = entrada.nextInt();
			if (!v.joga(linha, coluna, jogador))
				System.out.println(" Digite outra posicao ");
			else
				jogador = v.trocaJogador(jogador);
			System.out.println(v);
		}
		jogador = v.trocaJogador(jogador);
		if (v.haGanhador())
			System.out.println(jogador + " ganhou ");
		else
			System.out.println(" Ninguem Ganhou ");
	}

}