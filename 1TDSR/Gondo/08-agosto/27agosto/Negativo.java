	import br.com.fiap.imagens.BibliotecaImagens;
public class Negativo {
	
		public static void main(String[] args) {
			BibliotecaImagens bib = new BibliotecaImagens("D:/paisagem_lago.jpg");
			
			int [][] blue = bib.getBlue();
			int [][] red = bib.getRed();
			int[][] green = bib.getGreen();
			
			for(int i = 0; i < blue.length; i++) {
				for(int j = 0; j < blue[0].length; j++) {
					blue[i][j] = 255- blue[i][j];
					red[i][j]  = 255- red[i][j];
					green[i][j] = 255- green[i][j];
				}
			}
			
			bib.gravaArquivoColorido("D:/lagoInvertido.png", red, green, blue);
		}

		
	}
