import br.com.fiap.imagens.BibliotecaImagens;

public class Crop {

	public static void main(String[] args) {
		BibliotecaImagens bib = new BibliotecaImagens("D:/paisagem_lago.jpg");
		
		int [][] blue = bib.getBlue();
		int [][] red = bib.getRed();
		int[][] green = bib.getGreen();
		
		int[][] cropBlue = new int[300][500];
		int[][] cropRed = new int[300][500];
		int[][] cropGreen = new int[300][500];
		
		for(int i = 0; i < 300; i++) {
			for(int j = 0; j < 500; j++) {
				cropBlue[i][j] = blue[i+200][j+260];
				cropRed[i][j]  = red[i+200][j+260];
				cropGreen[i][j] = green[i+200][j+260];
			}
		}
		
		bib.gravaArquivoColorido("D:/lagoCrop.png", cropRed, cropGreen, cropBlue);
		
	}

	
}
