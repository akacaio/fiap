import br.com.fiap.imagens.BibliotecaImagens;

public class Vanish {
	
	public static void main(String[] args) {
		
		BibliotecaImagens bib = new BibliotecaImagens("D:/yao-ming.png");
		
		int[][] yao = bib.getPretoBranco();
		int row = yao.length;
		int col = yao[0].length;
		
		int[][] resp = new int[row][col];
		
		for(int i = 0; i < yao.length; i++) {
			for(int j = 0; j < yao[i].length; j++ ) {
			if(yao[i][j] == 255) {
				//pintar de branco (255) a posi��o i,j da matriz resp e sua vizinhan�a
				pintaVizinhos(i, j, resp);
				}
			}
		}
		
		bib.gravaArquivoPretoBranco("D:/yao-ming2.png", resp);
	}
	
	private static void pintaVizinhos(int i, int j, int[][] resp) {
		resp[i][j] = 255;
		
		// linha i-1
		try {
			resp[i-1][j-1] = 255;
		} catch (Exception e) {}
		
		try {
			resp[i-1][j] = 255;
		} catch(Exception e) {}
		
		try {
			resp[i-1][j+1] = 255;
		} catch(Exception e) {}
		
		// linha i
		try {
			resp[i][j-1] = 255;
		} catch (Exception e) {}
		
		try {
			resp[i][j+1] = 255;
		} catch(Exception e) {}
		
		// linha i +1	
		try {
			resp[i+1][j-1] = 255;
		} catch (Exception e) {}
		
		try {
			resp[i+1][j] = 255;
		} catch(Exception e) {}
		
		try {
			resp[i+1][j+1] = 255;
		} catch(Exception e) {}
	}


}
