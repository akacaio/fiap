import br.com.fiap.imagens.BibliotecaImagens;

public class FiltroBlackWhite {
	
	public static void main(String[] args) {
		
		BibliotecaImagens bib = new BibliotecaImagens("D:/natureza-morta.jpg");
		
		int[][] mat = bib.getCinza();
		
		int row = mat.length;
		int col = mat[0].length;
		
		int[][] resp = new int[row][col];
		
		for (int i = 0; i < resp.length; i++) {
			for (int j = 0; j < resp[0].length; j++) {
				if(mat[i][j] <= 200) {
					resp[i][j] = 0;
				} else {
					resp[i][j] = 255;
				}
			}
		}
		
		bib.gravaArquivoPretoBranco("D:/naturezaCinza.jpg", resp);
	}

}
