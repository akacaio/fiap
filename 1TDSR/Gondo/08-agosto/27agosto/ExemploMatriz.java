
public class ExemploMatriz {
	
	public static void main(String[] args) {
		
		int [][] matriz = new int[4][5];
		int valor = 1;
		
		for(int i = 0; i < matriz.length; i++) {
			for(int j = 0; j < matriz[0].length; j++) {
				matriz[i][j] = valor;
				valor++;
			//	System.out.print("Linha "+ i + "\nColuna " + j + "\n=> " + matriz[i][j] +"\n \n");
				System.out.print(matriz[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
