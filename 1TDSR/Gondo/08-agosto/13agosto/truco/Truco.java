package br.com.fiap.truco;

import br.com.fiap.baralho.Baralho;
import br.com.fiap.baralho.BaralhoTruco;
import br.com.fiap.baralho.Carta;

public class Truco {

	public static void main(String[] args) {
		
	Baralho b = new BaralhoTruco();
	b.embaralharCartas();
	
	Carta[] card = b.distribuirCartas(3);
	Carta vira = b.comprarCartas();
	
	Jogador player = new Jogador(card, "Caio", vira);
	Carta c = player.jogarCartas(null);
	System.out.println(c);
	

	}
}
