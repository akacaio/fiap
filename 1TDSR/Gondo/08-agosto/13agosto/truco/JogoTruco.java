package br.com.fiap.truco;

import javax.swing.JOptionPane;

import br.com.fiap.baralho.Baralho;
import br.com.fiap.baralho.BaralhoTruco;
import br.com.fiap.baralho.Carta;
import br.com.fiap.baralho.Naipe;

public class JogoTruco {
	
	public static int getPontos(Carta c, Carta vira) {
		
		if(isManilha(c, vira)) {
			
			if(c.getNaipe() == Naipe.PAUS) return 100;
			else if(c.getNaipe() == Naipe.COPAS) return 80;
			else if(c.getNaipe() == Naipe.ESPADA) return 60;
			else return 40;
			
		} else {				
		switch(c.getValor()) {
			
			case 1:
				return 12;
			case 2:
				return 15;
			case 3:
				return 20;
			case 11:
				return 9;
			case 12:
				return 8;
			case 13: 
				return 10;
			default:
				return c.getValor();
			
			}	
		}		
	}

	private static boolean isManilha(Carta c, Carta vira) {
		int valorManilha = 0;
		
		switch(vira.getValor()) {
			case 7:
				valorManilha = 12;
				break;
			case 11:
				valorManilha = 13;
				break;
			case 12:
				valorManilha = 11;
				break;
			case 13:
				valorManilha = 1;
				break;
			default:
				valorManilha = vira.getValor() + 1;
				break;
			}
		if(c.getValor() == valorManilha) {
			return true;
		} else {
			return false;	
		}
	}
	

	private static void imprime(Carta c, Carta d) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Carta jogador: ");
		sb.append(c);
		sb.append("\nCarta CPU: ");
		sb.append(d);
		sb.append("\n");
		JOptionPane.showMessageDialog(null, sb.toString());
		
	}

	public static void main(String[] args) {
		
		int placarJ = 0;
		int placarCPU = 0;
		
		Baralho b = new BaralhoTruco();
		b.embaralharCartas();
		
		Carta[] aux = b.distribuirCartas(3);
		Carta[] auxComp = b.distribuirCartas(3);
		Carta vira = b.comprarCartas();
		
		Jogador j = new Jogador(aux, "Caio", vira);
		Jogador cpu = new JogadorCpu(auxComp, "CPU", vira);
		
		while(placarJ < 2 && placarCPU < 2) {
		
		Carta c = j.jogarCartas(null);
		Carta d = cpu.jogarCartas(c);		
		
		int valorCartaP1 = getPontos(c, vira);
		int valorCartaP2 = getPontos(d, vira);
		imprime(c, d);
		
		if(valorCartaP1 >= valorCartaP2) placarJ++;
		else if(valorCartaP1 <= valorCartaP2) placarCPU++;
	
		}
		
		if(placarCPU > placarJ) System.out.println("CPU venceu!");
		else if(placarCPU < placarJ) System.out.println("Jogador venceu!");

	}
}
