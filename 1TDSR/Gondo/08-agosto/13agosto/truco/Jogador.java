package br.com.fiap.truco;

import javax.swing.JOptionPane;

import br.com.fiap.baralho.Carta;

public class Jogador {

	protected Carta[] mao;
	protected String nome;
	protected Carta vira;
	
	public Jogador(Carta[] mao, String nome, Carta vira) {
		this.mao = mao;
		this.nome = nome;
		this.vira = vira;
	}
	
	public Carta jogarCartas(Carta mesa) {
		String pos = JOptionPane.showInputDialog(imprimeCartas());
		Carta resp = this.mao[Integer.parseInt(pos)];
		this.mao[Integer.parseInt(pos)] = null;
		return resp;
	}
	
	public String imprimeCartas() {
		return mao[0] + " " + mao[1] + " " + mao[2] + "->" + vira;
	}
	
}
