package br.com.fiap.truco;

import br.com.fiap.baralho.Carta;

public class JogadorCpu extends Jogador {
	
	public JogadorCpu(Carta[] mao, String nome, Carta vira) {
		super(mao, nome, vira);
	}
	
	public Carta jogarCartas(Carta mesa) {
		Carta c = null;
		
		if(this.mao[0] != null) {
			c = this.mao[0];
			this.mao[0] = null;
		} else if(this.mao[1] != null) {
			c = this.mao[1];
			this.mao[1] = null;
		} else {
			c = this.mao[2];
			this.mao[2] = null;
		}
		
		return c;
	}
	

}
