package br.com.fiap.baralho;

public class BaralhoTruco extends Baralho {
	
	
	public BaralhoTruco() {
	
		this.monte = new Carta[40];
		int[] cartasValidas = { 1, 2, 3, 4, 5, 6, 7, 12, 11, 13 };
		int j = 0;
		
		for(int i = 0; i < cartasValidas.length; i++) {
			this.monte[j] = new Carta(cartasValidas[i],Naipe.PAUS);
			j++;
		}
		
		for(int i = 0; i < cartasValidas.length; i++) {
			this.monte[j] = new Carta(cartasValidas[i],Naipe.COPAS);
			j++;
		}
		
		for(int i = 0; i < cartasValidas.length; i++) {
			this.monte[j] = new Carta(cartasValidas[i],Naipe.ESPADA);
			j++;
		}
		
		for(int i = 0; i < cartasValidas.length; i++) {
			this.monte[j] = new Carta(cartasValidas[i],Naipe.OUROS);
			j++;
		}	
	}

}
