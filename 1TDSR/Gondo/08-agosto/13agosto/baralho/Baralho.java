package br.com.fiap.baralho;

import java.util.Random;

public class Baralho {
		
	protected Carta[] monte;
	protected int embaralhar;
	protected int topoBaralho;
	protected int cartasNaipe;
	
	public Baralho() {
		// serve para inicializar os atributos do objeto
		topoBaralho = 0;
		monte = new Carta[52];
		embaralhar = 30;
		cartasNaipe = 13;
		int x = 0;		

		for(int k = 1; k <= cartasNaipe; k++) {
		monte[x] = new Carta(k,Naipe.PAUS);
		x++;
		}
		
		
		for(int k = 1; k <= cartasNaipe; k++) {
		monte[x] = new Carta(k,Naipe.COPAS);
		x++;
		}
		
		for(int k = 1; k <= cartasNaipe; k++) {
		monte[x] = new Carta(k,Naipe.ESPADA);
		x++;
		}

		for(int k = 1; k <= cartasNaipe; k++) {
		monte[x] = new Carta(k,Naipe.OUROS);
		x++;
		}
	
	
	}
	
	public void embaralharCartas() {
		
		Random rand = new Random();
		
		for (int i = 0; i <= embaralhar; i++) {
			
		int n = rand.nextInt(monte.length);
		int j = rand.nextInt(monte.length);
		
		Carta card = monte[j];
		monte[j] = monte[i];
		monte[i] = card;
		
		}
	}
	
	public Carta comprarCartas() {
		topoBaralho++;
		return monte[topoBaralho - 1];
	}
	
	public Carta[] distribuirCartas(int quantidade) {
		
		Carta[] distribuida =  new Carta[quantidade];
		for(int i = 0; i < distribuida.length; i++) {
			distribuida[i] = comprarCartas();			
		}
		return distribuida;
	}
	
	public void cortarBaralho() {
		
	}

}
