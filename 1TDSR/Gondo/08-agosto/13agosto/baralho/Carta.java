package br.com.fiap.baralho;

public class Carta {
	
	private int valor;
	private Naipe naipe;
	
	public Carta(int valor, Naipe naipe) {
		super();
		this.valor = valor;
		this.naipe = naipe;
	}
	
	public int getValor() {
		return valor;
	}
	
	public Naipe getNaipe() {
		return naipe;
	}
	
	public String toString() {
		String v, n;
		v = Integer.toString(valor);
		
		switch(valor) {
		
		case 1:
			v = "A";
			break;
			
		case 11:
			v = "J";
			break;
			
		case 12:
			v = "Q";
			break;
		
		case 13:
			v = "K";
			break;
		}
		
		n = naipe.toString();
		
		if(naipe == Naipe.COPAS) n = "♥";
		if(naipe == Naipe.PAUS) n = "♣";
		if(naipe == Naipe.ESPADA) n = "♠";
		if(naipe == Naipe.OUROS) n = "♦";
		
		return v +" "+ n;
	}

}
