package br.com.fiap.exercicios;

import javax.swing.JOptionPane;

import br.com.fiap.baralho.Baralho;
import br.com.fiap.baralho.Carta;

public class Exercicio3 {
	
	public static void main(String[] args) {
		
	Baralho b = new Baralho();
	b.embaralharCartas();
	
	Carta p1 = b.comprarCartas();
	Carta p2 = b.comprarCartas();
	
	String res;
	
	if(p1.getValor() > p2.getValor()) {
		res = "Player 1 venceu";
	} else
		if(p1.getValor() < p2.getValor()) {
		res = "Player 2 venceu!";
	} else {
		res = "Empatou!";
	}
	
	JOptionPane.showMessageDialog(null, p1 + " x " + p2 + " => " +res);
	
	}
}
