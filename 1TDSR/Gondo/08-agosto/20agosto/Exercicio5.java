package br.com.fiap.exercicios;

import javax.swing.JOptionPane;

import br.com.fiap.baralho.Baralho;
import br.com.fiap.baralho.Carta;
import br.com.fiap.baralho.Naipe;

public class Exercicio5 {
	
	public static int getValorNaipe(Naipe c) {
		if(c == Naipe.PAUS) return 5;
		else if(c == Naipe.COPAS) return 4;
		else if(c == Naipe.ESPADA) return 3;
		else return 2;
	}
	
	public static void main(String[] args) {
	
	int rodadas = 0;
	int jogador1 = 0;
	int jogador2 = 0;
	int empate = 0;
		
	Baralho b = new Baralho();
	b.embaralharCartas();
	
	
	while(rodadas != 26) {
		
	
	Carta p1 = b.comprarCartas();
	Carta p2 = b.comprarCartas();
	
	if(p1.getValor() > p2.getValor()) {
		JOptionPane.showMessageDialog(null, "Rodada "+rodadas +"\nJogador 1 ganhou rodada \n" + p1 + " x " + p2);
		jogador1++;
	} else
		if(p1.getValor() < p2.getValor()) {
		JOptionPane.showMessageDialog(null, "Rodada "+rodadas +"\nJogador 2 ganhou rodada \n" + p1 + " x " + p2);
		jogador2++;
	} else {
		empate++;
		if(getValorNaipe(p1.getNaipe()) > getValorNaipe(p2.getNaipe())){
			JOptionPane.showMessageDialog(null, "Rodada "+rodadas +"\nJogador 1 ganhou rodada \n" + p1 + " x " + p2);
			jogador1++;
		} else
			if(getValorNaipe(p1.getNaipe()) < getValorNaipe(p2.getNaipe())) {
			JOptionPane.showMessageDialog(null, "Rodada "+rodadas +"\nJogador 2 ganhou rodada \n" + p1 + " x " + p2);
			jogador2++;
		}
	}
	rodadas++;
	}
	
	if(jogador1 > jogador2) {
		JOptionPane.showMessageDialog(null, "Jogador 1 ganhou a partida");
	} else
		if(jogador1 < jogador2) {
		JOptionPane.showMessageDialog(null, "Jogador 2 ganhou a partida");
		}
	
	JOptionPane.showMessageDialog(null, "Placar final: " + "P1 " + jogador1 + " x " + "P2 " + jogador2 + "\nRodadas empatadas: " + empate);
	}
}
