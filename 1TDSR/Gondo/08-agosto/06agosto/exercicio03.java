
public class exercicio03 {
	
	public static void main(String[] args) {
		
		boolean[] portas = new boolean[10];
		
		for(int p = 1; p < portas.length -1; p++) {
			for(int porta = 1; porta < portas.length; porta++) {
				if(porta % p == 0) {
					if(portas[porta] == false) {
						portas[porta] = true;
					} else {
						if(portas[porta] == true) {
							portas[porta] = false;
						}
					}
				}
			}
		}
		
		for(int a = 1; a < portas.length; a++) {
			System.out.println(portas[a]);
		}
	}

}
