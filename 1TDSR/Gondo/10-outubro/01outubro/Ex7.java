
public class Ex7 {
	
	
	public static void main(String[] args) {
		
		Ex7 obj = new Ex7();
		
		String bin = obj.converteIntToBin(75);
		System.out.println(bin);
		
		
	}

	public String converteIntToBin(int num) {
		
		int resto;
		int[] binarios = new int[8];
		int i = binarios.length - 1;
		
		while(num != 0) {
			resto = num % 2;
			binarios[i] = resto;
			i--;
			num = num / 2;
		}
		
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < binarios.length; j++) {
			sb.append(binarios[j]);
		}
		return sb.toString();
	}
	

}
