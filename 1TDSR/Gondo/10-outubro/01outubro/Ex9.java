
public class Ex9 {
	
	public static void main(String[] args) {
		
		String frase = "Caio";
		
		Ex9 obj = new Ex9();
		String s = obj.converteTextoParaBin(frase);
		System.out.println(s);
		
	}

	public String converteTextoParaBin(String frase) {
		
		int[] numeros = converteStringToInt(frase);
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < numeros.length; i++) {
			String binario = converteIntToBin(numeros[i]);
			sb.append(binario);
		}
		
		return sb.toString();
	}
	
	public int[] converteStringToInt(String frase) {
		
		int[] vet = new int[frase.length()];
		
		for (int i = 0; i < vet.length; i++) {
			vet[i] = frase.charAt(i);
		}
		
	return vet;
	}
	
	public String converteIntToBin(int num) {
	
		int resto;
		int[] binarios = new int[8];
		int i = binarios.length - 1;
		
		while(num != 0) {
			resto = num % 2;
			binarios[i] = resto;
			i--;
			num = num / 2;
		}
		
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < binarios.length; j++) {
			sb.append(binarios[j]);
		}
		return sb.toString();
	}

}
