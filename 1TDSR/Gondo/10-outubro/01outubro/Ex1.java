
public class Ex1 {
	
	
	public void inverte(int[] vetor) {
		
		int tam = vetor.length;
		int[] vetAux = new int[tam];
		
		for(int i = 1; i <= tam; i++) {
			vetAux[i-1] = vetor[tam - i];
		}		
		for(int i = 0; i < tam; i++) {
			vetor[i] = vetAux[i];
		}		
	}
	
	public static void main(String[] args) {
		int[] v = {2, 4, 7, 9, -5, 1};
		Ex1 obj = new Ex1();
		obj.inverte(v);
		for (int j = 0; j < v.length; j++)
			System.out.println(v[j]);
	}

}
