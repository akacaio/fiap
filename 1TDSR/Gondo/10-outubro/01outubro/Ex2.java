
public class Ex2 {
	
	public void inverte2(int[] vetor) {
		int ini = 0;
		int fim = vetor.length - 1;
		int aux;
		
		while(ini < fim) {
			aux = vetor[ini];
			vetor[ini] = vetor[fim];
			vetor[fim] = aux;
			
			ini++;
			fim--;
		}
	}
	
	public static void main(String[] args) {
		
		int[] v = {2, 4, 7, 9, -5, 1};
		Ex2 obj = new Ex2();
		obj.inverte2(v);
		for(int j = 0; j < v.length; j++) {
			System.out.println(v[j]);
		}
		
	}
	
}
